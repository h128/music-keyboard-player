extends AudioStreamPlayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


var sample_hz = 22050.0 # Keep the number of samples to mix low, GDScript is not super fast.
var pulse_hz = 440.0
#440.0 C4 1
var phase = 0.0
var delta_i=0
var playback: AudioStreamPlayback = null # Actual playback stream, assigned in _ready().

func _fill_buffer():
	#delta_i+=
	var increment = pulse_hz / sample_hz
	
	var to_fill = playback.get_frames_available()
	while to_fill > 0:
		playback.push_frame(Vector2.ONE * sin(phase * TAU)) # Audio frames are stereo.
		phase = fmod(phase + increment, 1.0)
		to_fill -= 1


func _process(_delta):
	#_fill_buffer()
	pass

func _ready():
	self.stream.mix_rate = sample_hz # Setting mix rate is only possible before play().
	playback = self.get_stream_playback()
	_fill_buffer() # Prefill, do before play() to avoid delay.
	self.play()
