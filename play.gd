extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
#A是1 S是2 D是3 以此类推 键盘布局 F是5
var C4=440
var time=0
#相邻音的比 *就是变高 /就是变小 1.059463
var D4=440*(1.059463*1.059463)
var E4=D4*1.059463*1.059463
var F4=E4*1.059463*1.059463
var G4=F4*1.059463*1.059463
var A4=G4*1.059463*1.059463
var B4=A4*1.059463*1.059463
var C5=B4*1.059463*1.059463
# Called when the node enters the scene tree for the first time.
func _ready():
	$AudioStreamPlayer.set_process(false)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_type_here_text_changed(new_text):
	print_debug(new_text)
	$to_type_text2.bbcode_text="[color=grey]"+new_text+"[/color]"
	$AudioStreamPlayer.set_process(true)
	$AudioStreamPlayer.play()
#	if "F" in new_text:
#		$AudioStreamPlayer.pulse_hz=550.0
#		$AudioStreamPlayer._fill_buffer()
#	if "A" in new_text:
#		$AudioStreamPlayer.pulse_hz=440.0
#		$AudioStreamPlayer._fill_buffer()
	if new_text.ends_with("A"):
		print_debug("end with A")
		$AudioStreamPlayer.pulse_hz=440.0
		$AudioStreamPlayer._fill_buffer()
	if new_text.ends_with("S"):
		print_debug("end with F")
		$AudioStreamPlayer.pulse_hz=D4
		$AudioStreamPlayer._fill_buffer()
		
	if new_text.ends_with("D"):
		print_debug("end with A")
		$AudioStreamPlayer.pulse_hz=E4
		$AudioStreamPlayer._fill_buffer()
		
	if new_text.ends_with("F"):
		print_debug("end with F")
		$AudioStreamPlayer.pulse_hz=F4
		$AudioStreamPlayer._fill_buffer()
	
	
	
	if new_text.ends_with("G"):
		print_debug("end with F")
		$AudioStreamPlayer.pulse_hz=G4
		$AudioStreamPlayer._fill_buffer()
	if new_text.ends_with("H"):
		print_debug("end with H")
		$AudioStreamPlayer.pulse_hz=A4
		$AudioStreamPlayer._fill_buffer()
	if new_text.ends_with("J"):
		print_debug("end with J")
		$AudioStreamPlayer.pulse_hz=B4
		$AudioStreamPlayer._fill_buffer()
	if new_text.ends_with("K"):
		print_debug("end with H")
		$AudioStreamPlayer.pulse_hz=C5
		$AudioStreamPlayer._fill_buffer()
#else :
#		$AudioStreamPlayer.set_process(false)
	pass # Replace with function body.


func _on_Timer_timeout():
	time+=1
	$TimeCoast.bbcode_text="[wave amp=50 freq=2]TimeCoast:"+str(time)+"[/wave]"
	pass # Replace with function body.


func _on_ok_pressed():
	$Panel.show()
	$Timer.stop()
	$Panel/TimeCoast2.bbcode_text="[wave amp=50 freq=2]TimeCoast:"+str(time)+"[/wave]"
	pass # Replace with function body.


func _on_back_pressed():
	get_tree().change_scene("res://Main.tscn")
	pass # Replace with function body.

#切换乐谱
func _on_ok2_pressed():
	$FileDialog.popup()
	pass # Replace with function body.


func _on_FileDialog_file_selected(path):
	$FileDialog.hide()
	var file=File.new()
	file.open(path,file.READ)
	var text=file.get_as_text()
	$to_type_text.text=text
	$to_type_text2.text=text
	file.close()
	pass # Replace with function body.
