extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var file_open
var file_save
var choosedFile=""
#结构 歌曲名称 乐谱文件路径 歌曲乐谱内容 每个数组互相对应
var toSaveSongStruct=[]
var songName
# Called when the node enters the scene tree for the first time.
func _ready():
	file_open=File.new()
	file_save=File.new()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_open_pressed():
	if OS.get_name()=="Android":
		$saveDialog.access=FileDialog.ACCESS_USERDATA
	$openDIalog.popup()
	pass # Replace with function body.
func _on_save_pressed():
	#OS.alert("请输入歌曲名称 此名称存入配置文件")
	#$PopupDialog.popup()
	if OS.get_name()=="Android":
		$saveDialog.access=FileDialog.ACCESS_USERDATA
	$saveDialog.popup()
	pass # Replace with function body.


func _on_openDIalog_file_selected(path):
	choosedFile=path
	print_debug(choosedFile)
	file_open.open(path,File.READ)
	var text=file_open.get_as_text()
	$TextEdit.text=text
	file_open.close()
	pass # Replace with function body.
func _on_saveDialog_file_selected(path):
	print_debug(path)
	file_save.open(path,File.WRITE_READ)
	var text=$TextEdit.text
	
	file_save.store_string(text)
	file_save.close()
	
	#toSaveSongStruct.append(path)
	
	#var update_config=File.new()
	#update_config.open("user://config.txt",File.WRITE_READ)
	#update_config.store_string(to_json({"songDetail":toSaveSongStruct}))
	
	pass # Replace with function body.


func _on_showFile_pressed():
	OS.shell_open(ProjectSettings.globalize_path("user://"))
	pass # Replace with function body.


func _on_showDemo_pressed():
	$TextEdit.text="AA GG HH G FF DD SS A GG FF DD S GG FF DD S AA GG HH G FF DD SS A"
	pass # Replace with function body.


func _on_back_pressed():
	get_tree().change_scene("res://Main.tscn")
	pass # Replace with function body.


func _on_cls_pressed():
	$TextEdit.text=""
	pass # Replace with function body.


func _on_ok_pressed():
	$PopupDialog.hide()
	toSaveSongStruct.append($PopupDialog/LineEdit.text)
	
	OS.alert("请选择一个路径来保存你的乐谱文件")
	
	$saveDialog.popup()
	
	pass # Replace with function body.
