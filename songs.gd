extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var file
var dir
# Called when the node enters the scene tree for the first time.
func _ready():
	file=File.new()
	#
	var path="user://"
	var dir = Directory.new()
	if dir.open(path) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		
		while file_name != "":
			#print_debug(file_name)
			if dir.current_is_dir():
				print("Found directory: " + file_name)
			else:
				print("Found file: " + file_name)
				file_name = dir.get_next()
			file_name=dir.get_next()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_play_pressed():
	get_tree().change_scene("res://play.tscn")
	pass # Replace with function body.


func _on_back_pressed():
	get_tree().change_scene("res://Main.tscn")
	pass # Replace with function body.
