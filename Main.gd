extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var file=File.new()
	
	if file.file_exists("user://config,txt"):
		file.open("user://config.text",File.READ_WRITE)
		var to_parse=file.get_as_text()
		var parse=parse_json(to_parse)
		print_debug(parse)
		pass
	else:
		file.open("user://config.txt",File.WRITE_READ)
		file.store_string("")
		file.close()
		
		
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_type_pressed():
	get_tree().change_scene("res://songs.tscn")
	pass # Replace with function body.


func _on_ustom_music_pressed():
	get_tree().change_scene("res://customMusic.tscn")
	pass # Replace with function body.


func _on_default_yin_pressed():
	OS.alert("以下为C4的简谱映射关系：A-1,S-2,D-3,F-4,G-5,H-6,J-7,K-8(高音1)","默认音符映射关系")
	pass # Replace with function body.
